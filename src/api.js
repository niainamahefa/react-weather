export const geoAPIOptions = {
    method: 'GET',
    headers: {
        'X-RapidAPI-Key': '8fafdea866mshbb2b6a58074e46fp1a4e09jsn15a183bc229a',
        'X-RapidAPI-Host': 'wft-geo-db.p.rapidapi.com'
    }
};

export const GEO_API_URL = "https://wft-geo-db.p.rapidapi.com/v1/geo";

export const WEATHER_API_URL = "https://api.openweathermap.org/data/2.5";
export const WEATHER_API_KEY = "dd9c5c3d223694f1eec3fc0a02f95431";